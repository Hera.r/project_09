from django.conf import settings
from django.db import models


class Ticket(models.Model):
	title = models.CharField(max_length=128)
	description = models.TextField(max_length=2048, blank=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	image = models.ImageField(upload_to="images", null=True, blank=True)
	time_created = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return f'{self.title}, {self.description}'


class Review(models.Model):
	ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
	headline = models.CharField(max_length=128)
	rating = models.PositiveSmallIntegerField(choices=[(i, i) for i in range(0, 6)], blank=False, default='Unspecified')
	body = models.TextField(max_length=8192, blank=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	time_created = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return f'{self.ticket.title}, {self.headline}, {self.body}, {self.body}'

	class Meta:
		unique_together = ('ticket', 'user')


class UserFollows(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, related_name="following")

	followed_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, related_name='followed_by')

	class Meta:
		"""
	ensures we don't get multiple UserFollows instances
	for unique user-user_followed pairs
	"""
		unique_together = ('user', 'followed_user',)
