from django.forms import ModelForm
from django import forms
from .models import Ticket, Review
from django.db import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


class UserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'password1', 'password2']


class TicketForm(ModelForm):
	class Meta:
		model = Ticket
		fields = ['title', 'description', 'image']


class ReviewNotResponseForm(ModelForm):
	class Meta:
		model = Review
		fields = '__all__'
		exclude = ['user', 'time_created', 'ticket']
		widgets = {'rating': forms.RadioSelect()}


class NameForm(forms.Form):
	user_name = forms.CharField(label='Username', max_length=100)
