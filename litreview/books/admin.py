from django.contrib import admin
from .models import Ticket, Review


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
	list_display = ('title', 'description', 'image')


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
	list_display = ('ticket', 'headline', 'body')
