from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('flux', views.flux, name='flux'),
    path('posts', views.posts, name='posts'),
    path('user_register', views.user_register, name='user_register'),
    path('create_ticket', views.create_ticket, name='create_ticket'),
    path('review_not_response', views.review_not_response, name='review_not_response'),
    path('update_ticket/<int:ticket_id>', views.update_ticket, name='update_ticket'),
    path('delete_ticket/<int:ticket_id>', views.delete_ticket, name='delete_ticket'),
    path('update_review/<int:review_id>', views.update_review, name='update_review'),
    path('delete_review/<int:review_id>', views.delete_review, name='delete_review'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('follow', views.follow, name='follow'),
    path('follow_user/<int:id_user>', views.follow_user, name='follow_user'),
    path('ticket_response/<int:ticket_id>', views.ticket_response, name='ticket_response'),
    path('unfollow_user/<int:unfollow_id>', views.unfollow_user, name='unfollow_user'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
