from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib.auth import logout
from django.contrib import messages
from .forms import UserForm, TicketForm, ReviewNotResponseForm, NameForm
from django.urls import reverse
from .models import Ticket, Review, UserFollows
from django.contrib.auth.decorators import login_required
from django.db.models import CharField, Value
from itertools import chain
from django.http import HttpResponse
from django.db.models import Exists, OuterRef
from django.shortcuts import get_object_or_404
from django.db.models import Q


def get_users_viewable_reviews(current_user):
	user_following = User.objects.filter(followed_by__user=current_user)
	reviews_of_all_users = Review.objects.filter(Q(user__in=user_following) | Q(user=current_user))
	return reviews_of_all_users


def get_users_viewable_tickets(current_user):
	user_following = User.objects.filter(followed_by__user=current_user)
	ticket_of_all_users = Ticket.objects.filter(Q(user__in=user_following) | Q(user=current_user))
	return ticket_of_all_users


@login_required(login_url='/accounts/login/')
def flux(request):
	reviews = get_users_viewable_reviews(request.user.id)
	reviews = reviews.annotate(content_type=Value('REVIEW', CharField()))
	tickets = get_users_viewable_tickets(request.user.id)
	tickets = tickets.annotate(content_type=Value('TICKET', CharField()))
	user_review_filter = Review.objects.filter(ticket=OuterRef('pk'), user=request.user)
	tickets = tickets.annotate(has_reviewed=Exists(user_review_filter))
	posts = sorted(chain(reviews, tickets), key=lambda post: post.time_created, reverse=True)
	context = {'posts': posts}
	return render(request, 'books/flux.html', context)


@login_required(login_url='/accounts/login/')
def posts(request):
	reviews = Review.objects.filter(user__id=request.user.id)
	reviews = reviews.annotate(content_type=Value('REVIEW', CharField()))
	tickets = Ticket.objects.filter(user__id=request.user.id)
	tickets = tickets.annotate(content_type=Value('TICKET', CharField()))
	user_review_filter = Review.objects.filter(ticket=OuterRef('pk'), user=request.user)
	tickets = tickets.annotate(has_reviewed=Exists(user_review_filter))
	posts = sorted(chain(reviews, tickets), key=lambda post: post.time_created, reverse=True)
	context = {'posts': posts}

	return render(request, 'books/posts.html', context)


def user_register(request):
	if request.user.is_authenticated:
		return redirect('flux')
	else:
		if request.method == 'POST':
			form = UserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				messages.success(request, 'Account was created for ' + user)
				return redirect('login')

	form = UserForm()
	context = {'form': form}
	return render(request, 'books/user_register.html', context)


def logout_view(request):
	logout(request)
	return redirect('login')


@login_required(login_url='/accounts/login/')
def create_ticket(request):
	if request.method == 'POST':
		form = TicketForm(request.POST, request.FILES)
		if form.is_valid():
			current_user = form.save(commit=False)
			current_user.user = request.user
			current_user.save()
			messages.success(request, 'The reviewer is published')
			return redirect(reverse('flux'))
		else:
			messages.error(request, 'An action was not successful or some other error occurred')
			return redirect(reverse('flux'))

	form = TicketForm()
	context = {'form': form}
	return render(request, 'books/create_ticket.html', context)


@login_required(login_url='/accounts/login/')
def review_not_response(request):

	if request.method == 'POST':
		ticket_formset = TicketForm(request.POST, request.FILES)
		reviews_formset = ReviewNotResponseForm(request.POST)
		if ticket_formset.is_valid() and reviews_formset.is_valid():
			user_current_ticket = ticket_formset.save(commit=False)
			user_current_reviews = reviews_formset.save(commit=False)
			user_current_ticket.user = request.user
			user_current_reviews.ticket = user_current_ticket
			user_current_reviews.user = request.user
			user_current_ticket.save()
			user_current_reviews.save()
			messages.success(request, 'the reviewer publish')
			return redirect('flux')
		else:
			messages.error(request, 'An action was not successful or some other error occurred')
			return redirect('flux')

	formset = TicketForm()
	form = ReviewNotResponseForm()
	context = {'formset': formset, 'form': form}
	return render(request, 'books/reviews_not_response.html', context)


@login_required(login_url='/accounts/login/')
def ticket_response(request, ticket_id):

	if request.method == 'POST':
		reviews_formset = ReviewNotResponseForm(request.POST)
		if reviews_formset.is_valid():
			user_current_reviews = reviews_formset.save(commit=False)
			instance_ticket = Ticket.objects.get(id=ticket_id)
			user_current_reviews.ticket = instance_ticket
			user_current_reviews.user = request.user
			user_current_reviews.save()
			messages.success(request, 'the reviewer publish')
			return redirect('flux')
		else:
			messages.error(request, 'An action was not successful or some other error occurred')
			return redirect('flux')
	current_ticket = Ticket.objects.get(id=ticket_id)
	form = ReviewNotResponseForm()
	context = {'current_ticket': current_ticket, 'form': form}
	return render(request, 'books/ticket_response.html', context)


@login_required(login_url='/accounts/login/')
def follow(request):
	context = {}
	if request.method == 'POST':
		form = NameForm(request.POST)
		if form.is_valid():
			name_user = form.cleaned_data.get('user_name')
			result_research = User.objects.filter(username__icontains=name_user)
			context['result_research'] = result_research
			redirect('follow')
		else:
			messages.error(request, 'An action was not successful or some other error occurred')
			return redirect('follow')
	form_name = NameForm()
	user_following = User.objects.filter(followed_by__user=request.user)
	followed = UserFollows.objects.filter(followed_user=request.user)
	context['form_name'] = form_name
	context['user_following'] = user_following
	context['followed'] = followed
	return render(request, 'snippets/follow.html', context)


@login_required(login_url='/accounts/login/')
def follow_user(request, id_user):
	if id_user:
		current_user = User.objects.get(id=request.user.id)
		following = UserFollows(user=current_user, followed_user_id=id_user)
		following.save()
		return redirect('follow')
	return HttpResponse('sorry')


@login_required(login_url='/accounts/login/')
def unfollow_user(request, unfollow_id):
	if unfollow_id:
		UserFollows.objects.filter(user=request.user, followed_user_id=unfollow_id).delete()
		return redirect('follow')
	return redirect('follow')


@login_required(login_url='/accounts/login/')
def update_ticket(request, ticket_id):
	instance = get_object_or_404(Ticket, id=ticket_id)
	form = TicketForm(request.POST or None, instance=instance)
	if form.is_valid():
		form.save()
		return redirect('flux')
	return render(request, 'books/update_ticket.html', {"form": form})


@login_required(login_url='/accounts/login/')
def delete_ticket(request, ticket_id):
	if ticket_id:
		Ticket.objects.get(id=ticket_id).delete()
		return redirect('posts')
	return redirect('posts')


@login_required(login_url='/accounts/login/')
def update_review(request, review_id):
	instance = get_object_or_404(Review, id=review_id)
	current_ticket = Ticket.objects.get(id=instance.ticket_id)
	form = ReviewNotResponseForm(request.POST or None, instance=instance)
	if form.is_valid():
		form.save()
		return redirect('flux')
	return render(request, 'books/update_review.html', {'current_ticket': current_ticket, "form": form})


@login_required(login_url='/accounts/login/')
def delete_review(request, review_id):
	if review_id:
		Review.objects.get(id=review_id).delete()
		return redirect('posts')
	return redirect('posts')
