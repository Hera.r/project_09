# Project_09
Ce projet un site qui permet de commercialiser un produit permettant à une communauté d'utilisateurs de consulter ou de solliciter une critique de livres à la demande.


## Pré-requis
Ce qu'il est requis pour commencer avec ce projet:

* Python3
* Poetry

## Installation
***
Installation Poetry

```
	Pour commencer, il faut installer Poetry

$ pip install --user poetry

Après l'installation, se deplacer dans le dossier courant "project_09" et faire:

$ poetry install 

En faisant cette commande, cela installe toute la dépendance du projet

l'étape suivante c'est d'activer le virutalenv de Poetry tout en restant dans le dossier courant:

$ poetry shell 

```

## Lien Documentation Poetry
***
* https://python-poetry.org/docs/cli/

## Démarrage

Se deplacer dans le dossier "litreview"

```
$ python3 manage.py migrate

$ python3 manage.py runserver

```

